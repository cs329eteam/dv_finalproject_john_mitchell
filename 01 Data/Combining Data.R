require("jsonlite")
require("RCurl")
require(dplyr)
require(ggplot2)
require(extrafont)

impexp <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from ImportsExports "'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_jtm3433', PASS='orcl_jtm3433', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE), ))
View(impexp)

labor <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from LaborRate "'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_jtm3433', PASS='orcl_jtm3433', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE), ))
View(labor)

df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="SELECT i.Country, i.Year, Description, Value, Flow, TradeUSD FROM ImportsExports i INNER JOIN LaborRate l ON (UPPER(i.Country) = UPPER(l.Country) AND i.Year  = l.Year) "'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_jtm3433', PASS='orcl_jtm3433', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE), ))
View(df)

clean = labor %>% dplyr::filter(COUNTRY == "UNITED STATES") %>% dplyr::filter(DESCRIPTION == "UNEMPLOYMENT") %>% tbl_df
View(clean)
ggplot() + 
  coord_cartesian() + 
  scale_x_continuous() +
  scale_y_continuous() +
  #facet_wrap(~SURVIVED) +
  #facet_grid(.~SURVIVED, labeller=label_both) + # Same as facet_wrap but with a label.
  #facet_grid(PCLASS~SURVIVED, labeller=label_both) +
  labs(title='') +
  labs(x="Year", y=paste("Unemployment")) +
  
  layer(data=clean, 
        mapping=aes(x=as.numeric(as.character(YEAR)), y=as.numeric(as.character(VALUE)), color=COUNTRY),  
        geom="point",
        #geom_params=list(), 
        stat="identity", 
        #stat_params=list(),
        #position=position_identity()
        position=position_jitter(width=0.3, height=0)
  )
