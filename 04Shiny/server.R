#server
require("jsonlite")
require("RCurl")
require(ggplot2)
require(dplyr)
require(shiny)
require(shinydashboard)
require(leaflet)
require(DT)

shinyServer(function(input, output) {
  
  df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="SELECT i.Country, i.Year, Description, Value, Flow, TradeUSD FROM ImportsExports i INNER JOIN LaborRate l ON (UPPER(i.Country) = UPPER(l.Country) AND i.Year  = l.Year) "'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_jtm3433', PASS='orcl_jtm3433', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE) ))
  
  #tab1
  ncount <- reactive({input$rdio})
  df1 <- eventReactive(input$rdio, {df %>% filter(DESCRIPTION=='UNEMPLOYMENT RATE')%>%filter(FLOW == ncount(), YEAR<"2008")%>%group_by(YEAR)%>%mutate(sum=sum(TRADEUSD))%>%mutate(avg=mean(VALUE))%>%ungroup()})
  output$Plot1 <- renderPlot({             
    plot <- ggplot() + 
      coord_cartesian() + 
      scale_x_continuous() +
      scale_y_continuous() +
      labs(title="Unemployment Trends") +
      labs(x=paste("Unemployment Rate"), y=paste("Imports/Exports(usd)")) +

      layer(data=df1(), 
            mapping=aes(x=as.numeric(VALUE), y=as.numeric(TRADEUSD)),
            stat="identity", 
            geom="point",
            position=position_identity()
      )
    plot
  })
  
  #tab2
  df22<-df %>%filter(YEAR>"1999", YEAR<"2006")%>%filter(FLOW != "Export", FLOW != "Re-Export")%>%group_by(COUNTRY)%>%mutate(sumim=sum(TRADEUSD))
  df23<-df %>%filter(YEAR>"1999", YEAR<"2006")%>%filter(FLOW != "Import", FLOW != "Re-Import")%>%group_by(COUNTRY)%>%mutate(sumex=sum(TRADEUSD))
  df24<-eventReactive(input$clicks2,{inner_join(df22, df23, by = "COUNTRY")%>%group_by(COUNTRY)%>%mutate(ratio=sumex-sumim)%>%distinct(ratio)})

  output$Plot2 <- renderPlot({             
    plot4 <- ggplot() + 
      coord_cartesian() + 
      scale_x_discrete() +
      scale_y_continuous() +
      labs(title="Exports - Imports for 2000-2005") +
      labs(x=paste("Country"), y=paste("Trade Differential (usd)")) +
      theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))+
      
      
      layer(data=df24(), 
            mapping=aes(x=COUNTRY, y=as.numeric(ratio)),
            stat="identity", 
            geom="bar",
            position=position_identity()
      )
    plot4
  })
  
  
  output$Plot4 <- renderPlot({             
    plot4 <- ggplot() + 
      coord_cartesian() + 
      scale_x_discrete() +
      scale_y_continuous() +
      labs(title="Sum of Exports by Country 2000-2005") +
      labs(x=paste("Country"), y=paste("Total Exports (usd)")) +
      theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))+
      
      
      layer(data=df2(), 
            mapping=aes(x=COUNTRY, y=as.numeric(sum)),
            stat="identity", 
            geom="bar",
            position=position_identity()
      )
    plot4
  })
  
  #tab3
  maxval <- reactive({input$range[2]})
  minval <- reactive({input$range[1]})
  df3<-eventReactive(input$clicks3, {df %>%filter(YEAR>1999)%>%filter(YEAR<2006)%>%filter(FLOW == "Import")%>%filter(DESCRIPTION=="UNEMPLOYMENT RATE")%>%group_by(COUNTRY)%>%mutate(sum = sum(TRADEUSD))%>%filter(sum>minval())%>%filter(sum<maxval())%>%mutate(sumunem=mean(VALUE))%>%ungroup()%>%mutate(mn=mean(VALUE))})

  output$Plot3 <- renderPlot({             
    plot3 <- ggplot() + 
      coord_cartesian() + 
      scale_x_discrete() +
      scale_y_continuous() +
      labs(title="Unemployment Rate Bounded By Imports(usd) 2000-2005") +
      labs(x=paste("Country"), y=paste("Total Imports (usd)")) +
      theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))+
      
      
      layer(data=df3(), 
            mapping=aes(x=COUNTRY, y=as.numeric(sumunem)),
            stat="identity", 
            geom="bar",
            position=position_identity()
      )+
      layer(data=df3(), 
            mapping=aes(x=COUNTRY, y=as.numeric(mn), color="red"),
            stat="identity", 
            geom="point",
            position=position_identity()
      )
    plot3
  })

  #tab4
  maxval1 <- reactive({input$range1[2]})
  minval1 <- reactive({input$range1[1]})

  df4<-eventReactive(input$clicks4, {df %>%filter(YEAR>1999)%>%filter(YEAR<2006)%>%filter(FLOW == "Export")%>%filter(DESCRIPTION=="UNEMPLOYMENT RATE")%>%group_by(COUNTRY)%>%mutate(sum=sum(TRADEUSD))%>%filter(sum>minval1())%>%filter(sum<maxval1())%>%mutate(sumunem=mean(VALUE))%>%ungroup()%>%mutate(mn=mean(VALUE))})
  output$Plot5 <- renderPlot({             
    plot5 <- ggplot() + 
      coord_cartesian() + 
      scale_x_discrete() +
      scale_y_continuous() +
      labs(title="Unemployment Rate Bounded By Exports(usd) 2000-2005") +
      labs(x=paste("Country"), y=paste("Unemployment Percentage")) +
      theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))+
      
      
      layer(data=df4(), 
            mapping=aes(x=COUNTRY, y=as.numeric(sumunem)),
            stat="identity", 
            geom="bar",
            position=position_identity()
      )+
      layer(data=df4(), 
            mapping=aes(x=COUNTRY, y=as.numeric(mn), color = "red"),
            stat="identity", 
            geom="point",
            position=position_identity()
      )
    plot5
  })
  
  #tab5
  df5 <- df %>% dplyr::arrange(TRADEUSD)
  output$table <- renderDataTable({datatable(df5)
  })
  
  
})
